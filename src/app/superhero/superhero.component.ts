import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ns-superhero',
  templateUrl: './superhero.component.html',
  styleUrls: ['./superhero.component.css']
})
export class SuperheroComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input()
  hero: any

}
