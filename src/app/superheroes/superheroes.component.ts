import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ns-superheroes',
  templateUrl: './superheroes.component.html',
  styleUrls: ['./superheroes.component.css']
})
export class SuperheroesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }


  heroes = [
    {
      "name" : "Anthony Edward 'Tony' Stark",
      "surname" : "Iron Man",
      "img" : "./assets/iron-man-icon.jpg",
      "type" : "Avenger",
      "role" : "founder",
      "favoriteColor" : "#c82f2e"
    },
    {
      "name" : "Steven 'Steve' Rogers",
      "surname" : "Captain America",
      "img" : "./assets/captain-icon.jpg",
      "type" : "Avenger",
      "role" : "member",
      "favoriteColor": "#336699"
    },
    {
      "name" : "Donald Blake",
      "surname" : "Thor",
      "img" : "./assets/thor-icon.jpg",
      "type" : "Avenger",
      "role" : "founder",
      "favoriteColor": "#77d2d6"
    },
    {
      "name" : "Robert 'Bruce' Banner",
      "surname" : "Hulk",
      "img" : "./assets/hulk-icon.jpg",
      "type" : "Avenger",
      "role" : "founder",
      "favoriteColor": "#58d68d"
    },
    {
      "name" : "Natasha Romanoff",
      "surname" : "Black Widow",
      "img" : "./assets/black-widow-icon.jpg",
      "type" : "Avenger",
      "role" : "member",
      "favoriteColor": "#273746"
    }
  ];

}
